# BDSL 3D Animation

## Introduction

Human contact requires effective communication, especially for people with speech and/or hearing disabilities who use sign language. However, current technology is incapable of automatically producing Bangla Sign Language (BdSL) animations from text or speech, limiting communication between sign and non-sign users. This study proposes a full solution a system that creates animated BdSL from Bangla text and vocal inputs. Our solution uses the HamNoSys notation system to precisely represent motions, which are subsequently animated using Signing Gesture Markup Language (SiGML). The major goal is to effortlessly transform Bangla voice and text into exact and credible sign language animations. Our suggested method takes Bangla voice and text inputs and maps them to the relevant SiGML files to create animated BdSL signs for composite numbers, alphabets, and sentences. We evaluate the system's performance using preset criteria such as animation correctness, fluency, and naturalness. The assessment shows that our system consistently generates realistic and accurate BdSL animations from voice and Bangla text inputs. Notably, we evaluate each form of input 100 times, and the system obtains an average interpretation accuracy of 94.75% for voice input and 97.50% for text input for all types of input, at a computational cost of 79.57 ms. Our proposed system primarily considers hand shape, palm orientation, movement, position, and expression/non-manual signals. This study takes a crucial step toward closing the communication gap between sign and non-sign communities. By offering open and inclusive channels, our approach has the potential to improve access to information and interaction for people with hearing impairments.

## Dataset

To prepare the system, we employed 94 classes of data. In this dataset, there are 13 Bangla numerical data classes, 36 Bangla alphabet data classes, and 41 Bangla word data classes. Every class of data contains different types of data like Hand Shape, Hand Orientation, Hand Movement, Notations, etc. These data were created in SiGML tags. Every class of data is unique and different from others. The system was prepared using BdSL. And BdSL is an uncommon and unique sign language, among others. That's why every class of data is unique and created by us. We search HamNoSys notations for Bangla alphabets, words, and numbers in English HamNoSys datasets (almost 6,000 data). However, we find only 20% of the data, which is quite similar to BdSL. 80% HamNoSys notation is created by us for BdSL and we modify the matching 20% notations. Then, we converted them into SiGML and made the data classes. This was a big challenge in our research. The dataset shows the 13 trained Bangla numeric data, by which the proposed system can generate any kind of numeric value animation. Also, the dataset shows the 36 trained Bangla alphabet data, by which the proposed system can spell any kind of Bangla word and sentence. And lastly, the figure shows the 10 examples of trained Bangla word data.

[Dataset Of Our System](https://zenodo.org/records/12152444?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6ImE3MmIxNTVhLWM0YTgtNDU1ZC1hYzVkLTYzNmVkYmQ0NWRkYSIsImRhdGEiOnt9LCJyYW5kb20iOiJlNjZlMGFmY2RlM2JhNGFjZjJlM2RmMWVjOGJlOGVjZiJ9.wPDph4DoWw_aMTGLlRp4P6YrBcDxD7YnP6TdS_xC2tpPcPVUpLb_d30Iay6ZSasQ2-EPHn1s1AEEXEfFr1XV8w&fbclid=IwZXh0bgNhZW0CMTAAAR2jwXmqvGEQH9PmaXZEhzKCvquGmUPYt5zIgZVNNRPZ15i36f4flfhDqII_aem_5CN3XFpkWhKhoRIKW39yNw)

## Requirements

To successfully run the BDSL 3D Animation project, the following requirements must be met:

### Software Requirements

- **XAMPP**: Version 7.4 or higher, with PHP 8.2 installed. XAMPP will provide the Apache server, MySQL database, and PHP required to run the application.
- **Composer**: Dependency Manager for PHP, for managing external libraries and frameworks.
- **Node.js**: Version 14 or higher, along with npm (Node Package Manager), for managing JavaScript dependencies.
- **Git**: For version control and collaboration.

### PHP Extensions

Ensure the following PHP extensions are enabled in your `php.ini` file:

- pdo_mysql
- mbstring
- openssl
- curl
- gd
- xml
- zip

### Browser

- A modern web browser like Google Chrome, Mozilla Firefox, Safari, or Microsoft Edge.

## Run

To launch a Laravel project, follow these steps:

1. **Start XAMPP**: Open XAMPP Control Panel and start Apache and MySQL modules.

2. **Clone the Project**: Clone the Laravel project repository from GitHub or any other version control system into your XAMPP's `htdocs` folder.

   ```bash
   git clone https://github.com/your-username/your-laravel-project.git
   ```

3. **Navigate to Project Directory**: Open a terminal or command prompt and navigate to your project directory.

   ```bash
   cd /path/to/your-laravel-project
   ```

4. **Install Dependencies**: Run Composer to install PHP dependencies defined in the composer.json file.

   ```bash
   composer install
   ```

5. **Start Laravel Development Server**: Finally, start the Laravel development server.

   ```bash
   php artisan serve
   ```
